use std::io;
use std::io::prelude::*;

enum MoveSource {
    Slot(u8),
    Stack(u8),
}

enum MoveDestination {
    Slot(u8),
    Pile(u8),
    Stack(u8),
}

pub fn menu() {
    let _source = choose_source();
    let _n = number_of_cards();
    let _destination = choose_destination();
}

fn choose_source() -> MoveSource {
    let mut buffer;
    let mut source;

    loop {
        buffer = String::new();
        println!("Select where to move FROM:");
        println!("1. Slot");
        println!("2. Stack");
        print!("> ");
        io::stdout().flush().ok().expect("failed to flush stdout");
        io::stdin()
            .read_line(&mut buffer)
            .expect("failed to read stdin");

        if buffer.trim() == "1" {
            source = MoveSource::Slot(0);
            break;
        } else if buffer.trim() == "2" {
            source = MoveSource::Stack(0);
            break;
        }
    }

    let name = match source {
        MoveSource::Slot(_) => "slot",
        MoveSource::Stack(_) => "stack",
    };

    loop {
        buffer = String::new();

        println!("Select which {} to move FROM:", name);
        print!("> ");
        io::stdout().flush().ok().expect("failed to flush stdout");
        io::stdin()
            .read_line(&mut buffer)
            .expect("failed to read stdin");

        let n: u8 = buffer.trim().parse().expect("failed to parse number");
        source = match source {
            MoveSource::Slot(_) => MoveSource::Slot(n),
            MoveSource::Stack(_) => MoveSource::Stack(n),
        };
        println!("moving FROM {} {}...", name, n);
        break;
    }

    source
}

fn number_of_cards() -> u8 {
    let mut buffer;
    let mut n = None;

    loop {
        buffer = String::new();
        println!("Select number of cards to move:");
        print!("> ");
        io::stdout().flush().ok().expect("failed to flush stdout");
        io::stdin()
            .read_line(&mut buffer)
            .expect("failed to read stdin");

        match buffer.trim().parse::<u8>() {
            Ok(value) => {
                n = Some(value);
                println!("moving {} card(s)...", value);
                break;
            }
            Err(_) => {
                println!("failed to parse number");
            }
        }
        break;
    }

    n.unwrap()
}

fn choose_destination() -> MoveDestination {
    let mut buffer;
    let mut destination;

    loop {
        buffer = String::new();
        println!("Select where to move TO:");
        println!("1. Slot");
        println!("3. Pile");
        println!("4. Stack");
        print!("> ");
        io::stdout().flush().ok().expect("failed to flush stdout");
        io::stdin()
            .read_line(&mut buffer)
            .expect("failed to read stdin");

        if buffer.trim() == "1" {
            destination = MoveDestination::Slot(0);
            break;
        } else if buffer.trim() == "2" {
            destination = MoveDestination::Pile(0);
            break;
        } else if buffer.trim() == "3" {
            destination = MoveDestination::Stack(0);
            break;
        }
    }

    let name = match destination {
        MoveDestination::Slot(_) => "slot",
        MoveDestination::Pile(_) => "pile",
        MoveDestination::Stack(_) => "stack",
    };

    loop {
        buffer = String::new();

        println!("Select which {} to move TO:", name);
        print!("> ");
        io::stdout().flush().ok().expect("failed to flush stdout");
        io::stdin()
            .read_line(&mut buffer)
            .expect("failed to read stdin");

        let n: u8 = buffer.trim().parse().expect("failed to parse number");
        destination = match destination {
            MoveDestination::Slot(_) => MoveDestination::Slot(n),
            MoveDestination::Pile(_) => MoveDestination::Pile(n),
            MoveDestination::Stack(_) => MoveDestination::Stack(n),
        };
        println!("moving TO {} {}...", name, n);
        break;
    }

    destination
}
