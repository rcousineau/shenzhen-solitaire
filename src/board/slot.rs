use std::fmt;
use Card;
use CardColor;

#[derive(Clone, PartialEq)]
pub struct CardSlot {
    pub id: u8,
    pub card: Option<Card>,
    pub dragon: Option<CardColor>,
}

impl CardSlot {
    pub fn new(id: u8) -> CardSlot {
        CardSlot {
            id,
            card: None,
            dragon: None,
        }
    }

    pub fn movable_cards(&self) -> Vec<Card> {
        if self.dragon.is_some() {
            return Vec::new();
        }
        match &self.card {
            Some(card) => vec![card.clone()],
            _ => Vec::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.card.is_none() && self.dragon.is_none()
    }

    pub fn can_accept_cards(&self, cards: &[Card]) -> bool {
        if cards.len() != 1 {
            return false;
        }
        if cards[0].is_flower() {
            return false;
        }
        self.is_empty()
    }

    pub fn remove_card(&mut self) {
        assert!(self.card.is_some(), "no card to remove from slot");

        self.card = None;
    }

    pub fn add_card(&mut self, card: Card) {
        assert!(self.is_empty(), "unable to add card to non-empty slot");

        self.card = Some(card);
    }

    pub fn slay_dragon(&mut self, color: CardColor) {
        assert!(self.is_empty(), "unable to slay dragon in non-empty slot");

        self.dragon = Some(color);
    }

    pub fn is_dragon_slain(&self) -> bool {
        self.dragon.is_some()
    }
}

impl fmt::Debug for CardSlot {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Slot {}", self.id)
    }
}

#[cfg(test)]
mod tests {
    use Card;
    use CardColor;
    use CardSlot;

    fn green_3() -> Card {
        Card::number(CardColor::Green, 3)
    }

    #[test]
    fn can_return_movable_cards() {
        let empty_slot = CardSlot::new(1);
        assert_eq!(empty_slot.movable_cards(), Vec::new());

        let mut dragon_slot = CardSlot::new(1);
        dragon_slot.slay_dragon(CardColor::Red);
        assert_eq!(dragon_slot.movable_cards(), Vec::new());

        let mut number_slot = CardSlot::new(1);
        number_slot.add_card(green_3());
        assert_eq!(number_slot.movable_cards(), vec![green_3()]);
    }
}
