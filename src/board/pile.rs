use std::fmt;
use Card;
use CardColor;

#[derive(Clone, PartialEq)]
pub struct CardPile {
    pub color: CardColor,
    pub top: Option<Card>,
}

impl CardPile {
    pub fn new(color: CardColor) -> CardPile {
        CardPile { color, top: None }
    }

    pub fn can_accept_cards(&self, cards: &[Card]) -> bool {
        if cards.len() != 1 {
            return false;
        }
        let card = &cards[0];
        if !card.is_number() {
            return false;
        }
        let color = card.color().unwrap().clone();
        if color != self.color {
            return false;
        }
        let value = card.value().unwrap();

        match &self.top {
            Some(top) => {
                let top_value = top.value().unwrap();
                (top_value + 1) == value
            }
            _ => value == 1,
        }
    }

    pub fn add_card(&mut self, card: Card) {
        self.top = Some(card);
    }
}

impl fmt::Debug for CardPile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} Pile", self.color)
    }
}
