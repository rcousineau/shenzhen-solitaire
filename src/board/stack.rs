use std::fmt;
use Card;

#[derive(Clone, PartialEq)]
pub struct CardStack {
    pub id: u8,
    pub cards: Vec<Card>,
}

impl CardStack {
    pub fn new(id: u8, cards: &[Card]) -> CardStack {
        CardStack {
            id,
            cards: cards.to_vec(),
        }
    }

    pub fn top(&self) -> Option<&Card> {
        self.cards.last()
    }

    pub fn movable_cards(&self) -> Vec<Card> {
        self.find_valid_movable_cards(self.cards.as_ref(), &[])
    }

    // search through `candidates` for a movable stack of cards
    fn find_valid_movable_cards(&self, candidates: &[Card], cards: &[Card]) -> Vec<Card> {
        // there are no candidates to search
        if candidates.is_empty() {
            return cards.to_vec();
        }
        let candidate = candidates.last().unwrap().clone();
        let last_index = candidates.len() - 1;

        if cards.is_empty() {
            let result: Vec<Card> = vec![candidate];
            return self.find_valid_movable_cards(&candidates[..last_index], result.as_ref());
        }

        let last_card = cards.last().unwrap();
        if last_card.can_stack_on(&candidate) {
            let mut result: Vec<Card> = vec![candidate];
            result.extend(cards.to_vec());
            return self.find_valid_movable_cards(&candidates[..last_index], &result);
        }
        cards.to_vec()
    }

    pub fn can_accept_cards(&self, cards: &[Card]) -> bool {
        match self.top() {
            Some(top) => cards[0].can_stack_on(top),
            _ => cards[0].is_number(),
        }
    }

    pub fn remove_cards(&mut self, count: usize) {
        let new_count = self.cards.len() - count;
        self.cards = self.cards[..new_count].to_vec();
    }

    pub fn add_cards(&mut self, cards: Vec<Card>) {
        self.cards.extend(cards);
    }
}

impl fmt::Debug for CardStack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Stack {}", self.id)
    }
}

#[cfg(test)]
mod tests {
    use Card;
    use CardColor;
    use CardStack;

    fn white_5() -> Card {
        Card::number(CardColor::White, 5)
    }

    fn red_5() -> Card {
        Card::number(CardColor::Red, 5)
    }

    fn white_4() -> Card {
        Card::number(CardColor::White, 4)
    }

    fn green_3() -> Card {
        Card::number(CardColor::Green, 3)
    }

    fn white_2() -> Card {
        Card::number(CardColor::White, 2)
    }

    #[test]
    fn can_return_movable_cards() {
        let stack_cards = vec![red_5(), green_3(), white_2()];
        let stack = CardStack::new(1, &stack_cards);
        let movable_cards = stack.movable_cards();

        assert_eq!(movable_cards, vec![green_3(), white_2()]);
    }

    #[test]
    fn can_detect_receivable_cards() {
        let stack_cards = vec![red_5()];
        let stack = CardStack::new(1, &stack_cards);

        let cards = vec![white_4(), green_3()];
        assert!(stack.can_accept_cards(&cards));
    }

    #[test]
    fn can_detect_receivable_card() {
        let stack_cards = vec![red_5()];
        let stack = CardStack::new(1, &stack_cards);

        let cards = vec![white_4()];
        assert!(stack.can_accept_cards(&cards));
    }

    #[test]
    fn can_detect_non_receivable_cards() {
        let stack_cards = vec![red_5()];
        let stack = CardStack::new(1, &stack_cards);

        let cards = vec![green_3(), white_2()];
        assert!(!stack.can_accept_cards(&cards));
    }

    #[test]
    fn can_detect_non_receivable_card() {
        let stack_cards = vec![red_5()];
        let stack = CardStack::new(1, &stack_cards);

        let cards = vec![white_5()];
        assert!(!stack.can_accept_cards(&cards));
    }
}
