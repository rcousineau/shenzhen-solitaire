pub use board::pile::CardPile;
pub use board::slot::CardSlot;
pub use board::stack::CardStack;
use Card;
use CardColor;
use Deck;
use PossibleMove;

pub mod pile;
pub mod slot;
pub mod stack;

#[derive(Clone, Debug)]
pub struct Board {
    slots: Vec<CardSlot>,
    flower: bool,
    piles: Vec<CardPile>,
    stacks: Vec<CardStack>,
}

impl Board {
    pub fn new(deck: Deck) -> Board {
        let piles = CardColor::colors()
            .into_iter()
            .map(move |color| CardPile::new(color))
            .collect();

        let cards = deck.cards();

        Board {
            slots: vec![CardSlot::new(0), CardSlot::new(1), CardSlot::new(2)],
            flower: false,
            piles,
            stacks: vec![
                CardStack::new(0, &cards[0..5]),
                CardStack::new(1, &cards[5..10]),
                CardStack::new(2, &cards[10..15]),
                CardStack::new(3, &cards[15..20]),
                CardStack::new(4, &cards[20..25]),
                CardStack::new(5, &cards[25..30]),
                CardStack::new(6, &cards[30..35]),
                CardStack::new(7, &cards[35..40]),
            ],
        }
    }

    pub fn from_str(input_string: String) -> Board {
        let deck = Deck::new(input_string);
        Board::new(deck)
    }

    pub fn slots(&self) -> &Vec<CardSlot> {
        self.slots.as_ref()
    }

    pub fn flower(&self) -> bool {
        self.flower
    }

    pub fn piles(&self) -> &Vec<CardPile> {
        self.piles.as_ref()
    }

    pub fn stacks(&self) -> &Vec<CardStack> {
        self.stacks.as_ref()
    }

    pub fn is_solved(&self) -> bool {
        let dragons_slain: Vec<bool> = self
            .slots
            .iter()
            .map(|slot| slot.is_dragon_slain())
            .collect();
        let dragons_slain: bool = dragons_slain.len() == 4;

        let stacks_with_cards: Vec<&CardStack> = self
            .stacks
            .iter()
            .filter(|stack| stack.top().is_none())
            .collect();
        let all_stacks_empty = stacks_with_cards.is_empty();

        dragons_slain && all_stacks_empty
    }

    pub fn execute_move(&mut self, m: &PossibleMove, slaying_dragon: bool) {
        match m {
            PossibleMove::Flower(from_stack) => {
                self.remove_stack_cards(from_stack.id, 1);
                self.flower = true;
            }
            PossibleMove::SlotToSlot(from_slot, card, to_slot) => {
                if !slaying_dragon {
                    panic!("doesn't make sense to move from slot to slot unless slaying dragon!");
                }
                self.remove_slot_card(from_slot.id);
                self.slay_dragon(to_slot.id, card.color().unwrap());
            }
            PossibleMove::SlotToPile(from_slot, card, to_pile) => {
                self.remove_slot_card(from_slot.id);
                self.add_card_to_pile(&to_pile.color, card);
            }
            PossibleMove::SlotToStack(from_slot, card, to_stack) => {
                self.remove_slot_card(from_slot.id);
                self.add_cards_to_stack(to_stack.id, vec![card.clone()]);
            }
            PossibleMove::StackToSlot(from_stack, card, to_slot) => {
                self.remove_stack_cards(from_stack.id, 1);
                self.add_card_to_slot(to_slot.id, card);
            }
            PossibleMove::StackToStack(from_stack, cards, to_stack) => {
                if from_stack.id == to_stack.id {
                    panic!("can't move cards onto their current stack!");
                }
                self.remove_stack_cards(from_stack.id, cards.len());
                self.add_cards_to_stack(to_stack.id, cards.clone());
            }
            PossibleMove::StackToPile(from_stack, card, to_pile) => {
                self.remove_stack_cards(from_stack.id, 1);
                self.add_card_to_pile(&to_pile.color, card);
            }
            PossibleMove::Dragon(slay_the_dragon) => {
                for s in slay_the_dragon {
                    &mut self.execute_move(s, true);
                }
            }
        }
    }

    fn remove_slot_card(&mut self, slot_id: u8) {
        for s in &mut self.slots.iter_mut() {
            if s.id == slot_id {
                s.remove_card();
                break;
            }
        }
    }

    fn remove_stack_cards(&mut self, stack_id: u8, count: usize) {
        for s in &mut self.stacks.iter_mut() {
            if s.id == stack_id {
                s.remove_cards(count);
                break;
            }
        }
    }

    fn add_card_to_pile(&mut self, pile_color: &CardColor, card: &Card) {
        for p in &mut self.piles.iter_mut() {
            if p.color == pile_color.clone() {
                p.add_card(card.clone());
                break;
            }
        }
    }

    fn add_card_to_slot(&mut self, slot_id: u8, card: &Card) {
        for s in &mut self.slots.iter_mut() {
            if s.id == slot_id {
                s.add_card(card.clone());
                break;
            }
        }
    }

    fn add_cards_to_stack(&mut self, stack_id: u8, cards: Vec<Card>) {
        for s in &mut self.stacks.iter_mut() {
            if s.id == stack_id {
                s.add_cards(cards);
                break;
            }
        }
    }

    fn slay_dragon(&mut self, slot_id: u8, color: &CardColor) {
        for s in &mut self.slots.iter_mut() {
            if s.id == slot_id {
                s.slay_dragon(color.clone());
                break;
            }
        }
    }
}

impl Default for Board {
    fn default() -> Board {
        let deck = Deck::default();
        Board::new(deck)
    }
}
