pub use solver::game_state::GameState;
pub use solver::solution::Solution;
use std::time::Duration;
use std::{process, thread};
use Board;

pub mod game_state;
pub mod solution;

pub struct Solver {}

impl Solver {
    pub fn find_solution(board: &Board) {
        let solution = Solution::new(board);
        let mut thread_handles = Vec::new();
        let possible_solutions = solution.possible_solutions();
        println!("INITIAL BRANCHES {}", possible_solutions.len());
        for possible_solution in possible_solutions {
            let s = possible_solution.clone();
            thread_handles.push(thread::spawn(|| {
                Solver::solve_spawn(s);
            }));
        }

        for handle in thread_handles {
            let id = handle.thread().id().clone();
            Solver::join_thread(handle);
            println!("joined thread {:?}", id);
        }
    }

    fn solve_spawn(solution: Solution) {
        Solver::solve(solution);
    }

    fn solve(solution: Solution) {
        match solution.state() {
            GameState::Won => {
                println!("found solution!");
                for m in solution.moves() {
                    println!("{:?}", m);
                    process::exit(0);
                }
            }
            GameState::Lost(message) => {
                println!("{}", message);
            }
            GameState::InProgress => {
                let possible_solutions = solution.possible_solutions();
                println!("BRANCHES {}", possible_solutions.len());
                for possible_solution in possible_solutions {
                    let s = possible_solution.clone();
                    thread::sleep(Duration::from_millis(1));
                    Solver::solve_spawn(s);
                }
            }
        }
    }

    fn join_thread(handle: thread::JoinHandle<()>) {
        match handle.join() {
            Err(err) => {
                println!("error joining thread: {:?}", err);
                process::exit(1);
            }
            _ => {}
        }
    }
}
