#[derive(Clone, Debug)]
pub enum GameState {
    InProgress,
    Lost(String),
    Won,
}
