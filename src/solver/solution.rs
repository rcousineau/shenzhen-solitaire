use std::thread;
use Board;
use Card;
use CardColor;
use CardSlot;
use GameState;
use PossibleMove;

#[derive(Clone)]
pub struct Solution {
    board: Board,
    moves: Vec<PossibleMove>,
    state: GameState,
}

impl Solution {
    pub fn new(board: &Board) -> Solution {
        Solution {
            board: board.clone(),
            moves: Vec::new(),
            state: GameState::InProgress,
        }
    }

    pub fn moves(&self) -> &Vec<PossibleMove> {
        &self.moves
    }

    pub fn state(&self) -> &GameState {
        &self.state
    }

    pub fn possible_solutions(&self) -> Vec<Solution> {
        if self.moves.len() > 10 {
            let mut result = self.clone();
            result.state = GameState::Lost(String::from("exceeded move limit"));
            return vec![result];
        }
        let possible_moves = Solution::possible_moves(&self.board);
        possible_moves
            .iter()
            .filter(|m| !self.moves.contains(m))
            .map(|m| {
                let move_num = self.moves.len() + 2;
                let thread_id = thread::current().id();
                println!(
                    "checking move {:?} ({}) - from thread {:?}",
                    m, move_num, thread_id
                );
                let mut board = self.board.clone();
                let mut moves = Vec::new();
                moves.extend(self.moves.clone());
                moves.push(m.clone());
                let state = Solution::check_move(&mut board, m);

                Solution {
                    board,
                    moves,
                    state,
                }
            })
            .collect()
    }

    fn check_move(board: &mut Board, possible_move: &PossibleMove) -> GameState {
        board.execute_move(possible_move, false);
        if board.is_solved() {
            GameState::Won
        } else if Solution::possible_moves(board).is_empty() {
            GameState::Lost(String::from("out of moves"))
        } else {
            GameState::InProgress
        }
    }

    fn possible_moves(board: &Board) -> Vec<PossibleMove> {
        let mut moves = Vec::new();
        moves.extend(Solution::possible_dragon_slaying_moves(board));
        moves.extend(Solution::possible_slot_moves(board));
        moves.extend(Solution::possible_stack_moves(board));
        moves
    }

    fn possible_slot_moves(board: &Board) -> Vec<PossibleMove> {
        let mut moves = Vec::new();

        for from_slot in board.slots() {
            let all_cards = from_slot.movable_cards();
            let combinations = Solution::all_card_combinations(all_cards);

            for cards in combinations {
                for to_pile in board.piles() {
                    if to_pile.can_accept_cards(&cards) {
                        moves.push(PossibleMove::SlotToPile(
                            from_slot.clone(),
                            cards[0].clone(),
                            to_pile.clone(),
                        ));
                    }
                }

                for to_stack in board.stacks() {
                    if to_stack.can_accept_cards(&cards) {
                        moves.push(PossibleMove::SlotToStack(
                            from_slot.clone(),
                            cards[0].clone(),
                            to_stack.clone(),
                        ));
                    }
                }
            }
        }

        moves
    }

    fn possible_stack_moves(board: &Board) -> Vec<PossibleMove> {
        let mut moves = Vec::new();

        for from_stack in board.stacks() {
            let all_cards = from_stack.movable_cards();

            if !all_cards.is_empty() && all_cards[0].is_flower() {
                moves.push(PossibleMove::Flower(from_stack.clone()));
            }

            let combinations = Solution::all_card_combinations(all_cards);

            for cards in combinations {
                for to_slot in board.slots() {
                    if to_slot.can_accept_cards(&cards) {
                        moves.push(PossibleMove::StackToSlot(
                            from_stack.clone(),
                            cards[0].clone(),
                            to_slot.clone(),
                        ));
                    }
                }

                for to_stack in board.stacks() {
                    if to_stack.can_accept_cards(&cards) {
                        moves.push(PossibleMove::StackToStack(
                            from_stack.clone(),
                            cards.clone(),
                            to_stack.clone(),
                        ));
                    }
                }

                for to_pile in board.piles() {
                    if to_pile.can_accept_cards(&cards) {
                        moves.push(PossibleMove::StackToPile(
                            from_stack.clone(),
                            cards[0].clone(),
                            to_pile.clone(),
                        ));
                    }
                }
            }
        }

        moves
    }

    // find all subsets within a stack of movable cards
    fn all_card_combinations(cards: Vec<Card>) -> Vec<Vec<Card>> {
        let mut cards = cards.clone();
        // reverse to build subsets from the back
        cards.reverse();
        let mut combinations = Vec::new();
        if cards.is_empty() {
            return combinations;
        }
        for l in 1..(cards.len() + 1) {
            let mut combination = cards.clone()[..l].to_vec();
            // reverse again to put back into original order
            combination.reverse();
            combinations.push(combination);
        }
        combinations
    }

    fn possible_dragon_slaying_moves(board: &Board) -> Vec<PossibleMove> {
        let mut moves = Vec::new();

        let empty_slots: Vec<&CardSlot> = board
            .slots()
            .iter()
            .filter(|slot| slot.is_empty())
            .collect();

        if empty_slots.is_empty() {
            return moves;
        }

        let empty_slot = empty_slots.first().unwrap();

        for color in CardColor::colors() {
            if Solution::can_slay_dragon(board, &color) {
                let mut slay_the_dragon = Vec::new();

                slay_the_dragon.extend(Solution::dragon_slaying_slot_moves(
                    board, &color, empty_slot,
                ));
                slay_the_dragon.extend(Solution::dragon_slaying_stack_moves(
                    board, &color, empty_slot,
                ));

                if slay_the_dragon.len() == 4 {
                    moves.push(PossibleMove::Dragon(slay_the_dragon))
                }
            }
        }

        moves
    }

    fn dragon_slaying_slot_moves(
        board: &Board,
        color: &CardColor,
        to_slot: &CardSlot,
    ) -> Vec<PossibleMove> {
        let mut moves = Vec::new();

        for slot in board.slots() {
            let cards = slot.movable_cards();
            match cards.last() {
                Some(card) => {
                    if card.is_dragon() && card.color().unwrap() == color {
                        moves.push(PossibleMove::SlotToSlot(
                            slot.clone(),
                            card.clone(),
                            to_slot.clone(),
                        ));
                    }
                }
                _ => {}
            }
        }

        moves
    }

    fn dragon_slaying_stack_moves(
        board: &Board,
        color: &CardColor,
        to_slot: &CardSlot,
    ) -> Vec<PossibleMove> {
        let mut moves = Vec::new();

        for stack in board.stacks() {
            let cards = stack.movable_cards();
            match cards.last() {
                Some(card) => {
                    if card.is_dragon() && card.color().unwrap() == color {
                        moves.push(PossibleMove::StackToSlot(
                            stack.clone(),
                            card.clone(),
                            to_slot.clone(),
                        ));
                    }
                }
                _ => {}
            }
        }

        moves
    }

    fn can_slay_dragon(board: &Board, color: &CardColor) -> bool {
        let mut all_movable_cards: Vec<Card> = Vec::new();
        for slot in board.slots() {
            all_movable_cards.extend(slot.movable_cards());
        }
        for stack in board.stacks() {
            all_movable_cards.extend(stack.movable_cards());
        }
        let movable_dragons: Vec<&Card> = all_movable_cards
            .iter()
            .filter(|card| card.is_dragon() && card.color().unwrap() == color)
            .collect();

        movable_dragons.len() == 4
    }
}
