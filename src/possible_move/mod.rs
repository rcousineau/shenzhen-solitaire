use Card;
use CardPile;
use CardSlot;
use CardStack;

#[derive(Clone, Debug, PartialEq)]
pub enum PossibleMove {
    Flower(CardStack),
    // should only be used for slaying dragons
    SlotToSlot(CardSlot, Card, CardSlot),
    SlotToPile(CardSlot, Card, CardPile),
    SlotToStack(CardSlot, Card, CardStack),
    StackToSlot(CardStack, Card, CardSlot),
    StackToStack(CardStack, Vec<Card>, CardStack),
    StackToPile(CardStack, Card, CardPile),
    Dragon(Vec<PossibleMove>),
}
