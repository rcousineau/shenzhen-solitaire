extern crate shenzhen_solitaire;

use shenzhen_solitaire::menu;
use shenzhen_solitaire::renderer;
use shenzhen_solitaire::Board;
// use shenzhen_solitaire::Solver;
// use std::{env, fs};

pub fn main() {
    // let args: Vec<String> = env::args().collect();
    // if args.len() < 2 {
    //     panic!("missing input filename parameter");
    // }
    // let input_filename = args[1].clone();
    // let input_contents = fs::read_to_string(input_filename).expect("failed to read input file");

    // let board = Board::new(input_contents);
    //    Solver::find_solution(&board);

    let board = Board::default();
    renderer::render(&board);
    menu::menu();
}
