use rand::seq::SliceRandom;
use rand::thread_rng;
use Card;
use CardColor;

#[derive(Clone, Debug)]
pub struct Deck {
    cards: Vec<Card>,
}

impl Deck {
    pub fn new(input_string: String) -> Deck {
        let cards = Deck::parse_input(&input_string);

        assert!(
            Deck::is_complete_set(&cards),
            "input file does not contain complete set of cards"
        );

        Deck { cards }
    }

    pub fn cards(&self) -> &Vec<Card> {
        self.cards.as_ref()
    }

    fn is_complete_set(cards: &Vec<Card>) -> bool {
        if cards.len() != 40 {
            return false;
        }

        let flower = Card::flower();
        if !cards.contains(&flower) {
            return false;
        }

        for color in CardColor::colors() {
            let dragon = Card::dragon(color.clone());
            let dragons: Vec<&Card> = cards
                .iter()
                .filter(|card| (*card).clone() == dragon.clone())
                .collect();
            if dragons.len() != 4 {
                return false;
            }

            for n in 1..=9 {
                let number = Card::number(color.clone(), n);
                if !cards.contains(&number) {
                    return false;
                }
            }
        }

        true
    }

    fn parse_input(input_string: &str) -> Vec<Card> {
        let grid = Deck::parse_input_grid(input_string);
        let grid = Deck::transpose_grid(grid);
        grid.iter()
            .flatten()
            .map(|card_str| Card::from_str(card_str))
            .collect()
    }

    fn parse_input_grid(input_string: &str) -> Vec<Vec<&str>> {
        let lines: Vec<&str> = input_string
            .split("\n")
            .filter(|line| !line.is_empty())
            .collect();

        lines
            .iter()
            .map(|line| {
                let cards: Vec<&str> = line.split_whitespace().collect();
                cards
            })
            .collect()
    }

    fn transpose_grid(grid: Vec<Vec<&str>>) -> Vec<Vec<&str>> {
        let height = grid.len();
        let width = grid[0].len();
        // create empty grid for inserting transposed values into
        let mut result = Vec::new();
        for _ in 0..width {
            result.push(vec![""; height]);
        }

        grid.iter().enumerate().for_each(|(i, line)| {
            line.iter().enumerate().for_each(|(j, card)| {
                result[j][i] = card.clone();
            });
        });
        result
    }
}

impl Default for Deck {
    fn default() -> Deck {
        let mut cards = vec![Card::flower()];
        for color in CardColor::colors() {
            for _ in 1..=4 {
                let dragon = Card::dragon(color);
                cards.push(dragon);
            }

            for n in 1..=9 {
                let number = Card::number(color, n);
                cards.push(number);
            }
        }

        assert!(Deck::is_complete_set(&cards));

        cards.shuffle(&mut thread_rng());

        Deck { cards }
    }
}

#[cfg(test)]
mod tests {
    use Deck;

    fn small_input_str() -> String {
        let mut result = String::new();
        result.push_str("RD F\n");
        result.push_str("W6 G4");
        result
    }

    fn full_input_str() -> String {
        let mut result = String::new();
        result.push_str("WD RD G2 G1 G4 G5 GD W9\n");
        result.push_str("RD R9 W3 RD R8 W4 R3 WD\n");
        result.push_str("W7 G8 W5 F  R2 RD GD GD\n");
        result.push_str("R7 G7 R5 R1 R6 W1 W2 G9\n");
        result.push_str("W6 R4 G3 G6 WD W8 GD WD");
        result
    }

    #[test]
    fn can_parse_input_grid() {
        let input_str = small_input_str();
        let card_grid = Deck::parse_input_grid(&input_str);
        assert_eq!(card_grid, vec![vec!["RD", "F"], vec!["W6", "G4"]]);
    }

    #[test]
    fn can_transpose_input_grid() {
        let input_grid = vec![vec!["RD", "F"], vec!["W6", "G4"]];
        let card_grid = Deck::transpose_grid(input_grid);
        assert_eq!(card_grid, vec![vec!["RD", "W6"], vec!["F", "G4"]]);
    }

    #[test]
    fn can_parse_valid_input() {
        Deck::new(full_input_str());
    }

    #[test]
    #[should_panic(expected = "input file does not contain complete set of cards")]
    fn can_detect_incomplete_input() {
        Deck::new(small_input_str());
    }
}
