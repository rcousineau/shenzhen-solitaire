#[derive(Clone, Debug, PartialEq)]
pub enum CardFlavor {
    Number,
    Dragon,
    Flower,
}
