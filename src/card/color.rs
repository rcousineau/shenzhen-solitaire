#[derive(Copy, Clone, Debug, PartialEq)]
pub enum CardColor {
    Red,
    Green,
    White,
}

impl CardColor {
    pub fn from_str(color_str: &str) -> CardColor {
        match color_str {
            "R" => CardColor::Red,
            "G" => CardColor::Green,
            "W" => CardColor::White,
            _ => panic!("invalid color {}", color_str),
        }
    }

    pub fn colors() -> Vec<CardColor> {
        vec![CardColor::Red, CardColor::Green, CardColor::White]
    }
}

#[cfg(test)]
mod tests {
    use CardColor;

    #[test]
    fn red_from_str() {
        assert_eq!(CardColor::from_str("R"), CardColor::Red);
    }

    #[test]
    fn green_from_str() {
        assert_eq!(CardColor::from_str("G"), CardColor::Green);
    }

    #[test]
    fn white_from_str() {
        assert_eq!(CardColor::from_str("W"), CardColor::White);
    }

    #[test]
    #[should_panic(expected = "invalid color X")]
    fn invalid_color_str() {
        CardColor::from_str("X");
    }
}
