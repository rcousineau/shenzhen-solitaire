pub use card::color::CardColor;
pub use card::flavor::CardFlavor;
use regex::Regex;
use std::fmt;

pub mod color;
pub mod flavor;

#[derive(Clone, PartialEq)]
pub struct Card {
    flavor: CardFlavor,
    color: Option<CardColor>,
    value: Option<u8>,
}

impl Card {
    pub fn from_str(input_str: &str) -> Card {
        let flower = Regex::new(r"^F$").unwrap();
        let dragon = Regex::new(r"^(\w)D").unwrap();
        let number = Regex::new(r"^(\w)(\d)").unwrap();

        if flower.is_match(input_str) {
            Card::flower()
        } else if dragon.is_match(input_str) {
            let cap = dragon.captures(input_str).unwrap();
            let color = CardColor::from_str(&cap[1]);
            Card::dragon(color)
        } else if number.is_match(input_str) {
            let cap = number.captures(input_str).unwrap();
            let color = CardColor::from_str(&cap[1]);
            let value = cap[2].parse::<u8>().unwrap();
            Card::number(color, value)
        } else {
            panic!("invalid card {} in input file", input_str)
        }
    }

    pub fn new(flavor: CardFlavor, color: Option<CardColor>, value: Option<u8>) -> Card {
        Card {
            flavor,
            color,
            value,
        }
    }

    pub fn flower() -> Card {
        Card {
            flavor: CardFlavor::Flower,
            color: None,
            value: None,
        }
    }

    pub fn dragon(color: CardColor) -> Card {
        Card {
            flavor: CardFlavor::Dragon,
            color: Some(color),
            value: None,
        }
    }

    pub fn number(color: CardColor, value: u8) -> Card {
        Card {
            flavor: CardFlavor::Number,
            color: Some(color),
            value: Some(value),
        }
    }

    pub fn color(&self) -> Option<&CardColor> {
        self.color.as_ref()
    }

    pub fn value(&self) -> Option<u8> {
        self.value
    }

    pub fn is_flower(&self) -> bool {
        self.flavor == CardFlavor::Flower
    }

    pub fn is_dragon(&self) -> bool {
        self.flavor == CardFlavor::Dragon
    }

    pub fn is_number(&self) -> bool {
        self.flavor == CardFlavor::Number
    }

    pub fn can_stack_on(&self, other: &Card) -> bool {
        if !self.is_number() || !other.is_number() {
            return false;
        }

        let color = self.color.clone().unwrap();
        let other_color = other.color.clone().unwrap();

        if color == other_color {
            return false;
        }

        let value = self.value().unwrap();
        let other_value = other.value().unwrap();

        (value + 1) == other_value
    }
}

impl fmt::Debug for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.flavor {
            CardFlavor::Flower => write!(f, "🌺"),
            CardFlavor::Dragon => match self.color {
                Some(CardColor::Red) => write!(f, "🀄"),
                Some(CardColor::Green) => write!(f, "🀅"),
                Some(CardColor::White) => write!(f, "🀆"),
                _ => write!(f, "?"),
            },
            CardFlavor::Number => {
                let value = self.value.as_ref().unwrap();
                let color = self.color.as_ref().unwrap();
                write!(f, "{} ({:?})", &value, &color)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use Card;
    use CardColor;

    #[test]
    fn flower_from_str() {
        let card = Card::from_str("F");
        assert_eq!(card, Card::flower());
        assert!(card.is_flower());
    }

    #[test]
    fn dragon_from_str() {
        let card = Card::from_str("GD");
        assert_eq!(card, green_dragon());
        assert!(card.is_dragon());
    }

    #[test]
    fn number_from_str() {
        let card = Card::from_str("R5");
        assert_eq!(card, red_5());
        assert!(card.is_number());
    }

    #[test]
    #[should_panic(expected = "invalid card XX in input file")]
    fn invalid_card_str() {
        Card::from_str("XX");
    }

    #[test]
    fn can_stack_alternating_colored_numbers_sequentially() {
        let red_5 = red_5();
        let white_6 = white_6();

        assert!(red_5.can_stack_on(&white_6));
        assert!(!white_6.can_stack_on(&red_5));
    }

    #[test]
    fn cannot_stack_same_colored_numbers() {
        let red_5 = red_5();
        let red_6 = red_6();

        assert!(!red_5.can_stack_on(&red_6));
        assert!(!red_6.can_stack_on(&red_5));
    }

    #[test]
    fn cannot_stack_dragons() {
        let green_dragon = green_dragon();
        let red_5 = red_5();

        assert!(!green_dragon.can_stack_on(&red_5));
        assert!(!red_5.can_stack_on(&green_dragon));
    }

    #[test]
    fn cannot_stack_flower() {
        let flower = Card::flower();
        let red_5 = red_5();

        assert!(!flower.can_stack_on(&red_5));
        assert!(!red_5.can_stack_on(&flower));
    }

    fn green_dragon() -> Card {
        Card::dragon(CardColor::Green)
    }

    fn red_5() -> Card {
        Card::number(CardColor::Red, 5)
    }

    fn red_6() -> Card {
        Card::number(CardColor::Red, 6)
    }

    fn white_6() -> Card {
        Card::number(CardColor::White, 6)
    }
}
