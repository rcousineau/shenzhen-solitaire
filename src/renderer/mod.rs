use crate::board::Board;
use crate::card::Card;

pub fn render(board: &Board) {
    print!("Slots: ");
    for slot in board.slots() {
        if slot.is_empty() {
            print!("_ ");
        } else if slot.card.is_some() {
            print!("{:?}", slot.card.as_ref().unwrap());
        } else if slot.dragon.is_some() {
            print!("{:?}", slot.dragon.unwrap());
        }
    }

    print!(" | Flower: ");

    if board.flower() {
        print!(" {:?} ", Card::flower());
    } else {
        print!(" _ ");
    }

    print!(" | Piles: ");

    for pile in board.piles() {
        if pile.top.is_none() {
            print!("_ ");
        } else {
            print!("{:?} ", pile.top.as_ref().unwrap());
        }
    }

    println!();
    println!();

    for (i, _) in board.stacks().iter().enumerate() {
        print!("{:^14}|", i);
    }

    println!();

    for _ in board.stacks() {
        print!("{:-^14}|", "-");
    }

    println!();

    for i in 0..40 {
        // build row by grabbing i-th item from each stack
        let row: Vec<Option<&Card>> = board
            .stacks()
            .iter()
            .map(|stack| {
                if stack.cards.len() > i {
                    Some(&stack.cards[i])
                } else {
                    None
                }
            })
            .collect();
        // only print if row is non-empty
        if !row.iter().flatten().collect::<Vec<&&Card>>().is_empty() {
            for card in row {
                if card.is_some() {
                    print!("{:^14}|", format!("{:?}", card.unwrap()));
                } else {
                    print!("{:^14}|", " ");
                }
            }
            println!();
        }
    }

    println!();
}
